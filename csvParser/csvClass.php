<?php
class Csv {

	public $delimiter = ',';
	public $enclosure = '"';

	public function decode($data) {
		$enclosed = false;
		$cellData = false;
		$currentRow = 0;
		$result = array();
		
		if(is_readable($data)) {
			$data = $this->readfile($data);
		}
		
		$data = preg_replace("/\r|\n/i","\r\n", $data);
		$result[$currentRow] = array();

		for($i = 0; $i < strlen($data); $i++) { 
			$ch = $data{$i};
			$nch = $data{$i+1};

			if($enclosed) {
				if($ch == $this->enclosure && $nch == $this->enclosure) {
					$cellData .= $this->enclosure;
					$i++;
				} else if($ch == $this->enclosure && $nch == $this->delimiter ) {
					$enclosed = false;
				} else if($data{$i-1} != $this->enclosure && $ch == $this->enclosure && ($nch == "\r"|| $nch == null) && ($data{$i+2} == "\n" || $data{$i+2} == null)) {
					$enclosed = false;
				} else {
					$cellData .= $ch;
				}
			} else {			
				if($ch == $this->delimiter) {
					$result[$currentRow][] = $cellData;
					$cellData = false;
				} else if($ch == $this->enclosure) {
					$enclosed = true;
				} else if($ch == "\r" && $nch == "\n") {
					$result[$currentRow][] = $cellData;
					$cellData = false;
					$currentRow++;
					$i++;
				} else {
					$cellData .= $ch;
				}
			}
		}
		$result[$currentRow][] = $cellData;
		
		return $result;
	}
	
	protected function readfile($file) {
		$fh = fopen($file, 'r');
		while (!feof($fh)) {
			$data .= fread($fh, 8192);
		}
		fclose($fh);
		
		return $data;
    }

}